<?php

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommissionController extends Controller
{
    /**
     * Get commisions for a given user.
     *
     * @Route("/commissions/{userId}", name="userCommissions")
     * @Method({"GET"})
     */
    public function userCommissions($userId)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        // User not found
        if ($user == null) {
            return $this->json(['errors' => 'Bad credentials.'], Response::HTTP_NOT_FOUND);
        }

        $commissions = [];
        foreach ($user->getCommissions() as $commission) {
            array_push($commissions, $commission->jsonSerialize());
        }

        return $this->json(compact('commissions'), 200, $this->getGenerationDateHeader());
    }

    private function getGenerationDateHeader()
    {
        return [
            'Generation-Date' => (new \DateTime())->format('Y-m-d H:i:s'),
        ];
    }
}
