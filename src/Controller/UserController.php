<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\EventSubscriber\UserLoggedInSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;

class UserController extends Controller
{
    /**
     * Register an user.
     *
     * @Route("/user/register", name="register")
     * @Method({"POST"})
     */
    public function register(Request $request, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        // We instanciate a new User object and set the values based on the request
        $user = new User();
        $user->setEmail($request->request->get('email', null));
        $user->setPassword($request->request->get('password', null));
        $user->setName($request->request->get('name', null));
        $user->setProfileUrl($request->request->get('profile_url', null));
        $user->setCreationDate(new \DateTime());

        // We validate the request
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return $this->json(['errors' => $errorsString], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json($user->jsonSerialize());
    }

    /**
     * Log an user in.
     *
     * @Route("/user/login", name="login")
     * @Method({"POST"})
     */
    public function login(Request $request, EntityManagerInterface $entityManager)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        $user = $repository->findOneBy([
            'email' => $request->request->get('email', null),
            'password' => $request->request->get('password', null),
        ]);

        // User not found
        if ($user == null) {
            return $this->json(['errors' => 'Bad credentials.'], Response::HTTP_NOT_FOUND);
        }

        // We dispatch a new UserLoggedIn event (send email)
        $dispatcher = new EventDispatcher();
        $subscriber = new UserLoggedInSubscriber();
        $dispatcher->addSubscriber($subscriber);

        // We update the lastLogin column for the given user
        $user->setLastLogin(new \DateTime());
        $entityManager->flush();

        return $this->json($user->jsonSerialize());
    }

    /**
     * Get user's informations.
     *
     * @Route("/user/{userId}", name="userInfos")
     * @Method({"GET"})
     */
    public function userInfos($userId)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        // User not found
        if ($user == null) {
            return $this->json(['errors' => 'Bad credentials.'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($user->jsonSerialize(), 200, $this->getGenerationDateHeader());
    }

    private function getGenerationDateHeader()
    {
        return [
            'Generation-Date' => (new \DateTime())->format('Y-m-d H:i:s'),
        ];
    }
}
