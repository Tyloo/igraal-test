<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommissionRepository")
 */
class Commission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     */
    private $cashback;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Merchant", inversedBy="commissions")
     * @ORM\JoinColumn(name="idMerchant", nullable=false)
     */
    private $idMerchant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commissions")
     * @ORM\JoinColumn(name="idUser", nullable=false)
     */
    private $idUser;

    public function getId()
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCashback(): ?float
    {
        return $this->cashback;
    }

    public function setCashback(float $cashback): self
    {
        $this->cashback = $cashback;

        return $this;
    }

    public function getIdMerchant(): ?Merchant
    {
        return $this->idMerchant;
    }

    public function setIdMerchant(?Merchant $idMerchant): self
    {
        $this->idMerchant = $idMerchant;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'date' => $this->date->format('Y-m-d H:i:s'),
            'cashback' => $this->cashback,
            'idMerchant' => $this->getIdMerchant()->getId(),
            'idUser' => $this->getIdUser()->getId(),
        ];
    }
}
