<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserLoggedInSubscriber implements EventSubscriberInterface
{
    const NAME = 'igraal.user.loggedin';

    public function onIgraalUserLoggedin($event)
    {
        // We send an email
        $this->sendEmail();
    }

    public static function getSubscribedEvents()
    {
        return [
           self::NAME => 'onIgraalUserLoggedin',
        ];
    }

    private function sendEmail()
    {
        sleep(1);
    }
}
